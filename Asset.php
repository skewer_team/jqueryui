<?php
namespace skewer\ext\jqueryui;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends  AssetBundle{

    public $sourcePath = '@vendor/skewer_team/jqueryui/web/';
    public $css = [
    ];
    public $js = [
        'jquery-ui.min.js',
        'jquery-ui-tabs.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}